class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  # before_action :authenticate

  def authenticate
    return unless Rails.env == 'production'
    authenticate_or_request_with_http_basic do |username, password|
      username == "poppy" && password == "doitlive"
    end
  end

end

class HomeController < ApplicationController

  before_filter :check_caching

  def formula
    @title = "Formula"
  end

  def home
    @title = "We make smart, beautifully designed products that think for you."
  end

  def legal
    @title = "Legal"
  end

  def pet
    @title = "Pet"
  end

  def pour_over
    @title = "Pour-Over"
  end

  protected

  def check_caching
    set_cache_control_headers if ENV['SET_CACHE_CONTROL_HEADERS'] == "true"
  end

end

module Api
  module V1
    class EmailsController < QuirkyApi::Base
      def create
        @email = Email.where(email: params[:email]).first_or_initialize
        @email.save!
        respond_with(email: @email.email)
      end
    end
  end
end

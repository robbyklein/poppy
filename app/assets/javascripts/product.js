function init_product_js() {

  $(document).ready(function() {
    /* Scrolls to next section */
    $('.down').click(function(){
      $('html,body').animate({
        scrollTop: $(".feature").offset().top
      });
    })

    // Shows text on plus hover
    $( ".plus" ).hover(
      function() {
        $(this).addClass('p-active');
      }, function() {
        $(this).removeClass('p-active');
      }
    );

    // Fixed Header
    $(window).scroll(function(){
      var pos = $(this).scrollTop()
      var box = $('.feature').offset().top
      if (pos >= box) {
        $('#fixed-nav').fadeIn(200)
      } else {
        $('#fixed-nav').fadeOut(200)
      }
    })


    $("#screen-slides").owlCarousel({
        slideSpeed : 100,
        paginationSpeed : 100,
        singleItem:true,
        autoPlay:3000,
        transitionStyle : "fade",
        mouseDrag: false,
        addClassActive: true,
        touchDrag: false,
        dots: false
    });

  });

};

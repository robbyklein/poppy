$(document).ready(function() {
  
  $('.signup-scroll').click(function(){
    $('html,body').animate({
      scrollTop: $("#footer-email").offset().top
    });
  })

  $('#mobile-menu').click(function(){
    $('#master-navigation').slideToggle()
  })

  $('#mobile-menu-2').click(function(){
    $('#fixed-nav ul').toggle()
  })

  $('#mobile-menu-3').click(function(){
    $('#product-navigation').slideToggle()
  })

})

function init_home_js() {

  $(document).ready(function() {

    // Creates Slider
    $("#slider").owlCarousel({
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        autoPlay:3000,
        transitionStyle : "fade",
        mouseDrag: false,
        addClassActive: true,
        touchDrag: false
    });

    // Stores Slider Data
    var owl = $(".owl-carousel").data('owlCarousel');

    // Scrolls Down to next section
    $('.down').click(function(){
      $('html,body').animate({
        scrollTop: $("#our-products").offset().top
      });
    })

    // Triggers Formula Video
    flag = 0;

    $(window).scroll(function () {
      var pos = $(this).scrollTop()
      var formula = $('#formula-video').offset().top - 100
      if (pos > formula && flag == 0) {
        flag = 1
        $('#formula-video')[0].play();
      }
    });


    // Triggers Pet Video
    flag2 = 0;

    $(window).scroll(function () {
      var pos = $(this).scrollTop()
      var formula = $('#pet-video').offset().top - 100
      if (pos > formula && flag2 == 0) {
        flag2 = 1
        $('#pet-video')[0].play();
      }
    });

    // Triggers Pour-over Video
    flag3 = 0;

    $(window).scroll(function () {
      var pos = $(this).scrollTop()
      var formula = $('#pour-over-video').offset().top - 100
      if (pos > formula && flag3 == 0) {
        flag3 = 1
        $('#pour-over-video')[0].play();
      }
    });

    // Fixed Header
    $(window).scroll(function(){
      var pos = $(this).scrollTop()
      var box = $('#our-products').offset().top
      if (pos >= box) {
        $('#fixed-nav').fadeIn(200)
      } else {
        $('#fixed-nav').fadeOut(200)
      }
    })

    // Handle scroll links.
    $('.scroll').click(function(e) {
      e.preventDefault();
      $('html,body').animate({
        scrollTop: $("#" + $(e.target).data('scroll-to')).offset().top
      });
      window.location.hash = $(e.target).data('hash');
    });

  });


  // Checks for hash and scrolls
  jQuery(window).load(function() {
    var a = $('[data-hash="' + window.location.hash.replace('#', '') + '"]');
    if (a.length == 0) return;
    $('html,body').animate({
      scrollTop: $("#" + $(a).data('scroll-to')).offset().top
    });
  });

};

$(document).ready(function() {

  // Success handler.

  var onSuccess = function(request, response) {
    $('#email-error').hide();
    $('#email-form input').val('');
    $('#email-form').hide()
    $('.default').hide()
    $('#email-success').show();
  };

  // Error handler.

  var onError = function(response) {
    $('#email-success').hide();
    if (response.status == 400) {
      $('#email-submit').addClass('error')
      $('#email-error').show()
      return;
    }

    $('#email-error').text('An error occured, please try again.').show();
  };

  // Hide notifications in keypress.

  $('#email-form input').keydown(function() {
    $('#email-error').hide();
    $('#email-success').hide();
  });

  // Removes error class on re-focus

  $('#email-input').focus(function(){
    $('#email-submit').removeClass('error')
    $('#email-error').hide()
  })

  // Handle form submission.

  $('#email-form').submit(function(e) {
    e.preventDefault();
    $.ajax({
      url: '/api/v1/emails',
      type: 'post',
      data: {
        email: $(this).find('input').val()
      },
      success: onSuccess,
      error: onError
    });
  });

});

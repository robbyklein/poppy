# config/unicorn.rb

worker_processes Integer(ENV['WEB_CONCURRENCY'] || 3)
listen (ENV['PORT']), backlog: Integer(ENV['UNICORN_BACKLOG'] || 20)
timeout 15
preload_app true

after_fork do |server, worker|
  ActiveRecord::Base.establish_connection if defined?(ActiveRecord)
end

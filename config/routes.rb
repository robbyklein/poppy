Rails.application.routes.draw do

  get 'pour-over', to: 'home#pour_over'
  get 'pet', to: 'home#pet'
  get 'formula', to: 'home#formula'
  get 'legal', to: 'home#legal'

  namespace :api, format: 'json' do
    namespace :v1 do
      resources :emails, only: [:create]
    end
  end

  root 'home#home'

end
